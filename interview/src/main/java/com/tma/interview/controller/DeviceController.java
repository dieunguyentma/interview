package com.tma.interview.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tma.interview.model.Device;

@Path("/device")
public abstract class DeviceController {

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public abstract List<Device> getAll();

}
