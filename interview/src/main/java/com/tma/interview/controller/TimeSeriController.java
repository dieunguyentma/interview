package com.tma.interview.controller;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.tma.interview.model.AggregationType;
import com.tma.interview.model.MetricType;
import com.tma.interview.model.Sequence;
import com.tma.interview.model.TimeSeri;

@Path("/seri")
public abstract class TimeSeriController {


	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public abstract List<TimeSeri> get(
			@QueryParam("deviceId") Long deviceId, 
			@QueryParam("from") String from, 
			@QueryParam("to") String to, 
			@QueryParam("type") MetricType type);
	
//	@GET
//	@Path("/types")
//	@Produces({ MediaType.APPLICATION_JSON })
//	public abstract List<String> getAllTypes();
	
	@GET
	@Path("/numbericTypes")
	@Produces({ MediaType.APPLICATION_JSON })
	public abstract List<String> getNumbericTypes();

	@GET
	@Path("/duration")
	@Produces({ MediaType.APPLICATION_JSON })
	public abstract Map<String, Double> getDurationData(
			@QueryParam("deviceId") Long deviceId, 
			@QueryParam("from") String from, 
			@QueryParam("to") String to, 
			@QueryParam("type") MetricType type,
			@QueryParam("aggregationType") AggregationType aggregationType);
	
	@GET
	@Path("/aggregationTypes")
	@Produces({ MediaType.APPLICATION_JSON })
	public abstract AggregationType[] getAllAggregationTypes();
	
	@GET
	@Path("/sequences")
	@Produces({ MediaType.APPLICATION_JSON })
	public abstract Sequence getSequences(
			@QueryParam("deviceId") Long deviceId, 
			@QueryParam("from") String from, 
			@QueryParam("to") String to);
}
