package com.tma.interview.controller.impl;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tma.interview.controller.DeviceController;
import com.tma.interview.dao.DeviceDAO;
import com.tma.interview.model.Device;

@Component
public class DeviceControllerImpl extends DeviceController {

	@Autowired
	private DeviceDAO deviceDAO;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Device> getAll() {
		List<Device> devices = deviceDAO.getAll();
		return devices;
	}

}
