package com.tma.interview.controller.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tma.interview.controller.TimeSeriController;
import com.tma.interview.dao.DeviceDAO;
import com.tma.interview.dao.TimeSeriDAO;
import com.tma.interview.model.APIException;
import com.tma.interview.model.AggregationType;
import com.tma.interview.model.DateFilter;
import com.tma.interview.model.Device;
import com.tma.interview.model.MetricType;
import com.tma.interview.model.Sequence;
import com.tma.interview.model.TimeSeri;
import com.tma.interview.service.AggregationFactory;
import com.tma.interview.service.AggregationHelper;
import com.tma.interview.service.SequenceBuilder;
import com.tma.interview.util.DateTimeUtil;

@Component
public class TimeSeriControllerImpl extends TimeSeriController {

	@Autowired
	private TimeSeriDAO timeSeriDAO;

	@Autowired
	private DeviceDAO deviceDAO;

	private DateFilter getDateFilter(String from, String to) {
		Date fromDate = null;
		try {
			fromDate = DateTimeUtil.DATE_TIME_FORMAT.parse(from);
		} catch (ParseException e) {
			throw new APIException("From date wrong format");
		}

		Date toDate = null;
		try {
			toDate = DateTimeUtil.DATE_TIME_FORMAT.parse(to);
		} catch (ParseException e) {
			throw new APIException("To date wrong format");
		}

		return new DateFilter(fromDate, toDate);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<TimeSeri> get(Long deviceId, String from, String to, MetricType type) {
		Device device = deviceDAO.getById(deviceId);
		if (device == null) {
			throw new APIException("Device not found");
		}

		DateFilter dateFilter = getDateFilter(from, to);
		return timeSeriDAO.get(device, type, dateFilter);
	}

//	@Override
//	public List<String> getAllTypes() {
//		return timeSeriDAO.getAllTypes();
//	}

	@Override
	public List<String> getNumbericTypes() {
		List<String> numbericTypes = new ArrayList<>();

		MetricType[] values = MetricType.values();
		for (MetricType value : values) {
			if (value.isNumberic()) {
				numbericTypes.add(value.name());
			}
		}

		return numbericTypes;
	}

	@Override
	public Map<String, Double> getDurationData(Long deviceId, String from, String to, MetricType type, AggregationType aggregationType) {
		Device device = deviceDAO.getById(deviceId);
		if (device == null) {
			throw new APIException("Device not found");
		}
		
		if (!type.isNumberic()) {
			throw new APIException("Metric type not supported, only numberic type is supported.");
		}

		DateFilter dateFilter = getDateFilter(from, to);
		
		Map<String, List<Double>> data = new HashMap<String, List<Double>>();
		
		List<TimeSeri> series = timeSeriDAO.get(device, type, dateFilter);
		for (TimeSeri seri : series) {
			Date timestamp = seri.getTimestamp();
			String date = DateTimeUtil.DATE_ONLY_FORMAT.format(timestamp);
			Double newValue = Double.parseDouble(seri.getValue());

			List<Double> existedValues = data.get(date);
			if (existedValues == null) {
				List<Double> newList = new ArrayList<>();
				newList.add(newValue);

				data.put(date, newList);
			} else {
				existedValues.add(newValue);
			}
		}
		
		return aggregate(data, aggregationType);
	}

	private Map<String, Double> aggregate(Map<String, List<Double>> data, AggregationType aggregationType) {
		AggregationHelper helper = AggregationFactory.findAggregation(aggregationType);

		Map<String, Double> rs = new HashMap<>();

		Set<String> keys = data.keySet();
		for (String key : keys) {
			List<Double> values = data.get(key);
			Double aggregated = helper.doAggregate(values);
			rs.put(key, aggregated);
		}

		return rs;
	}

	@Override
	public AggregationType[] getAllAggregationTypes() {
		return AggregationType.values();
	}

	@Override
	public Sequence getSequences(Long deviceId, String from, String to) {
		Device device = deviceDAO.getById(deviceId);
		if (device == null) {
			throw new APIException("Device not found");
		}

		DateFilter dateFilter = getDateFilter(from, to);
		List<TimeSeri> series = timeSeriDAO.get(device, MetricType.SENSOR_DATA_GPS_LOCATION_ADDRESS, dateFilter);

		SequenceBuilder builder = new SequenceBuilder();

		for (TimeSeri seri : series) {
			String value = seri.getValue();
			if (!value.isEmpty() && !value.equals("[ Undecoded ]")) {
				String[] adds = value.split(",");
				if (adds.length == 3) {
					builder.add(new Sequence(adds[0].trim()), new Sequence(adds[1].trim()), new Sequence(adds[2].trim()));
				}
			}
		}

		Sequence sequence = new Sequence("root");
		sequence.setChildren(builder.getData());
		
		return sequence;
	}
}
