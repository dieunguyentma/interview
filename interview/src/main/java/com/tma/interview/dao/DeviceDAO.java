package com.tma.interview.dao;

import com.tma.interview.model.Device;

public interface DeviceDAO extends GenericDAO<Device> {

}
