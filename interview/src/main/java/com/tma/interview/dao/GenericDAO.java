package com.tma.interview.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

public interface GenericDAO<T extends Serializable> {
	public Criteria getCriteria();

	public List<T> getAll();

	public T getById(long id);
}
