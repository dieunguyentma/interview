package com.tma.interview.dao;

import java.util.List;

import com.tma.interview.model.DateFilter;
import com.tma.interview.model.Device;
import com.tma.interview.model.MetricType;
import com.tma.interview.model.TimeSeri;

public interface TimeSeriDAO extends GenericDAO<TimeSeri> {

	public List<TimeSeri> get(Device device, MetricType type, DateFilter dateFilter);

	public List<String> getAllTypes();
	
}
