package com.tma.interview.dao.impl;

import org.springframework.stereotype.Component;

import com.tma.interview.dao.DeviceDAO;
import com.tma.interview.model.Device;

@Component
public class DeviceDAOImpl extends GenericDAOImpl<Device> implements DeviceDAO {

	public DeviceDAOImpl() {
		super(Device.class);
	}

}
