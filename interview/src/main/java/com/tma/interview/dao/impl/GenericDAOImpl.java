package com.tma.interview.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tma.interview.dao.GenericDAO;

@SuppressWarnings("unchecked")
public class GenericDAOImpl<T extends Serializable> implements GenericDAO<T> {

	private Class<T> _class;

	public GenericDAOImpl(Class<T> _class) {
		this._class = _class;
	}

	@Autowired
	protected SessionFactory sessionFactory;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Criteria getCriteria() {
		return getSession().createCriteria(_class);
	}

	@Override
	public List<T> getAll() {
		Criteria criteria = getCriteria();
		return criteria.list();
	}

	@Override
	public T getById(long id) {
		return (T) getSession().load(_class, id);
	}

}
