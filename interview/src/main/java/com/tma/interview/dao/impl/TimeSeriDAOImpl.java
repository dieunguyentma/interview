package com.tma.interview.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.tma.interview.dao.TimeSeriDAO;
import com.tma.interview.model.DateFilter;
import com.tma.interview.model.Device;
import com.tma.interview.model.MetricType;
import com.tma.interview.model.TimeSeri;

@Component
@SuppressWarnings("unchecked")
public class TimeSeriDAOImpl extends GenericDAOImpl<TimeSeri> implements TimeSeriDAO {

	public TimeSeriDAOImpl() {
		super(TimeSeri.class);
	}

	@Override
	public List<TimeSeri> get(Device device, MetricType type, DateFilter dateFilter) {
		Criteria criteria = getCriteria();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		if (device != null) {
			criteria.add(Restrictions.eq(TimeSeri.DEVICE, device));
		}
		if (type != null) {
			criteria.add(Restrictions.eq(TimeSeri.METRIC_TYPE, type));
		}
		criteria.add(Restrictions.between(TimeSeri.TIMESTAMP, dateFilter.getFrom(), dateFilter.getTo()));

		criteria.addOrder(Order.asc(TimeSeri.TIMESTAMP));
		
		return criteria.list();
	}

	@Override
	public List<String> getAllTypes() {
		Criteria criteria = getCriteria();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		criteria.setProjection(Projections.projectionList().add(Projections.groupProperty(TimeSeri.METRIC_TYPE)));
		return criteria.list();
	}

}
