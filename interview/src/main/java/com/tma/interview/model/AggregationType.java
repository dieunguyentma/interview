package com.tma.interview.model;

public enum AggregationType {
	SUM, AVG, MIN, MAX
}
