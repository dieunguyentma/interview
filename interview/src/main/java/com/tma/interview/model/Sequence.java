package com.tma.interview.model;

import java.util.ArrayList;
import java.util.List;

public class Sequence {
	private String name;
	private Long size = 0L;
	private List<Sequence> children = new ArrayList<>();

	public Sequence() {
	}

	public Sequence(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Sequence> getChildren() {
		return children;
	}

	public void setChildren(List<Sequence> children) {
		this.children = children;
	}

	public void addChild(Sequence child) {
		this.children.add(child);
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sequence other = (Sequence) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Sequence [name=" + name + ", size=" + size + ", children=" + children + "]";
	}

}
