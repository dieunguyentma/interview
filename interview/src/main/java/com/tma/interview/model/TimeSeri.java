package com.tma.interview.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.tma.interview.util.DateTimeSerializer;

@Entity(name = "time_series_data")
public class TimeSeri implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String VALUE = "value";
	public static final String DEVICE = "device";
	public static final String TIMESTAMP = "timestamp";
	public static final String METRIC_TYPE = "metricType";

	@Id
	@GeneratedValue
	private long id;

	@Enumerated(EnumType.STRING)
	private MetricType metricType;

	@JsonSerialize(using = DateTimeSerializer.class)
	private Date timestamp;
	private String value;

	@JsonIgnore
	@JoinColumn(name = "deviceId")
	@OneToOne(fetch = FetchType.LAZY)
	private Device device;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MetricType getMetricType() {
		return metricType;
	}

	public void setMetricType(MetricType metricType) {
		this.metricType = metricType;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	@Override
	public String toString() {
		return "TimeSeri [id=" + id + ", metricType=" + metricType + ", timestamp=" + timestamp + ", value=" + value + ", device=" + device + "]";
	}

}
