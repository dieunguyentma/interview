package com.tma.interview.service;

import com.tma.interview.model.AggregationType;

public class AggregationFactory {
	public static AggregationHelper findAggregation(AggregationType type) {
		if (type == null) {
			return new SumAggregation();
		}
		
		switch (type) {
		case AVG:
			return new AvgAggregation();

		case MAX:
			return new MaxAggregation();

		case MIN:
			return new MinAggregation();

		case SUM:
			return new SumAggregation();

		default:
			return new SumAggregation();
		}
	}
}
