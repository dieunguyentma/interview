package com.tma.interview.service;

import java.util.List;

public abstract class AggregationHelper {
	public abstract Double doAggregate(List<Double> values);
}
