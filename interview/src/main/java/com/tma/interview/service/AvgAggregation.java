package com.tma.interview.service;

import java.util.List;

public class AvgAggregation extends AggregationHelper {

	@Override
	public Double doAggregate(List<Double> values) {
		Double rs = 0.0;
		for (Double v : values) {
			rs += v;
		}
		return rs / values.size();
	}

}
