package com.tma.interview.service;

import java.util.Collections;
import java.util.List;

import com.tma.interview.model.APIException;

public class MaxAggregation extends AggregationHelper {

	@Override
	public Double doAggregate(List<Double> values) {
		if (values.isEmpty()) {
			throw new APIException("Max aggregate values error, values is empty.");
		}

		return Collections.max(values);
	}

}
