package com.tma.interview.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tma.interview.model.Sequence;

public class SequenceBuilder {

	private Map<String, Sequence> countries = new HashMap<>();
	private Map<String, Sequence> cities = new HashMap<>();
	private Map<String, Sequence> streets = new HashMap<>();

	private List<Sequence> data = new ArrayList<>();

	public synchronized void add(Sequence street, Sequence city, Sequence country) {
		String streetKey = country.getName() + "_" + city.getName() + "_" + street.getName();
		street.setSize(1L);

		Sequence sStreet = streets.get(streetKey);
		if (sStreet != null) {
			Long size = sStreet.getSize();
			sStreet.setSize(size + 1);
		} else {
			String cityKey = country.getName() + "_" + city.getName();
			Sequence sCity = cities.get(cityKey);

			if (sCity != null) {
				sCity.addChild(street);

				streets.put(streetKey, street);
			} else {
				String countryKey = country.getName();
				Sequence sContry = countries.get(countryKey);
				if (sContry != null) {
					city.addChild(street);
					sContry.addChild(city);

					streets.put(streetKey, street);
					cities.put(cityKey, city);
				} else {
					city.addChild(street);
					country.addChild(city);
					data.add(country);

					countries.put(countryKey, country);
					streets.put(streetKey, street);
					cities.put(cityKey, city);
				}
			}
		}
	}

	public List<Sequence> getData() {
		return data;
	}
}
