package com.tma.interview.util;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.tma.interview.model.APIException;

@Provider
public class APIExceptionMapper implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		if (exception instanceof APIException) {
			return Response.status(400).entity(exception.getMessage()).build();
		}

		exception.printStackTrace();
		return Response.status(500).entity("Server Internal error").build();
	}
}