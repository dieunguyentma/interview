package com.tma.interview.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateTimeUtil {
	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat(DATE_TIME_PATTERN);
	
	public static final String DATE_ONLY_PATTERN = "yyyy-MM-dd";
	public static final DateFormat DATE_ONLY_FORMAT = new SimpleDateFormat(DATE_ONLY_PATTERN);
}
