BASE_URL = "http://localhost:8080/api";
TIME_RANGE_FORMAT = "HH:mm, MMM DD, YYYY";
SERVER_FORMAT = "YYYY-MM-DDTHH:mm:ssZZ";

var elements = [];
var chart0, chart1;
var deviceId0, deviceId1, deviceId2, type0, type1, aggregationType, from0, from1, from2, to0, to1, to2; 

$( document ).ready(function() {
	initControllers();
	
	chart0 = drawChart('chart0', "N/A"); // empty chart
	
	drawHeatMap();
	chart1 = drawChart('chart1', "N/A"); // empty chart
	
	setTimeout(function(){ updateHeatMap(); }, 1000);
});

function updateFilters() {
	console.log("updateFilters");
	
	deviceId0 = $("#device_list0").find(":selected").val();
	deviceId1 = $("#device_list1").find(":selected").val();
	deviceId2 = $("#device_list2").find(":selected").val();

	type0 = $("#type_list0").find(":selected").val();
	type1 = $("#type_list1").find(":selected").val();
	
	aggregationType = $("#aggregationType").find(":selected").val();
}

function updateChartElements(data) {
	elements = [];
	$.each(data, function(key, item) {
		ele = [moment(item.timestamp).valueOf(), parseInt(item.value, 10)];
		elements.push(ele);
	});
};

function updateHeatMap() {
	console.log("updateHeatMap");
	
	updateFilters();
	getDurationData(deviceId1, from1, to1, type1, aggregationType, function(data) {
		updateDataHeatMap(data);
	})
}

function initDeviceFilter(id, data, callback) {
	var ele = $("#" + id);
	ele.select2();
	
	$.each(data, function(key, item) {
		ele.append("<option value=" + item.id + ">" + item.name + "</option>");
	});
	// init event
	ele.change(function() {
		callback();
	});
}

function initTypeFilter(id, data, callback) {
	var ele = $("#" + id);
	ele.select2();
	
	$.each(data, function(key, item) {
		ele.append("<option value=" + item + ">" + item + "</option>");
	});
	// init event
	ele.change(function() {
		callback();
	});
}

function initAggregateFilter(id, data, callback) {
	var ele = $("#" + id);
	ele.select2();
	
	$.each(data, function(key, item) {
		ele.append("<option value=" + item + ">" + item + "</option>");
	});
	// init event
	ele.change(function() {
		callback();
	});
}

function initControllers() {
	initTimeRange("daterange0", function(start, end) {
		$('#daterange0 span').html(start.format(TIME_RANGE_FORMAT) + ' - ' + end.format(TIME_RANGE_FORMAT));
		
		from0 = encodeURIComponent(start.format(SERVER_FORMAT));
		to0 = encodeURIComponent(end.format(SERVER_FORMAT));
		updateChart0();
	});
	
	initTimeRange("daterange1", function(start, end) {
		$('#daterange1 span').html(start.format(TIME_RANGE_FORMAT) + ' - ' + end.format(TIME_RANGE_FORMAT));
		
		from1 = encodeURIComponent(start.format(SERVER_FORMAT));
		to1 = encodeURIComponent(end.format(SERVER_FORMAT));
		updateHeatMap();
	});
	
	initTimeRange("daterange2", function(start, end) {
		$('#daterange2 span').html(start.format(TIME_RANGE_FORMAT) + ' - ' + end.format(TIME_RANGE_FORMAT));
		
		from2 = encodeURIComponent(start.format(SERVER_FORMAT));
		to2 = encodeURIComponent(end.format(SERVER_FORMAT));
		updateSequencesChart();
	});
	
	getAllDevices(function(data){
		initDeviceFilter("device_list0", data, updateChart0);
		initDeviceFilter("device_list1", data, updateHeatMap);
		initDeviceFilter("device_list2", data, updateSequencesChart);
	});

	getNumbericTypes(function(data){
		initTypeFilter("type_list0", data, updateChart0);
		initTypeFilter("type_list1", data, updateHeatMap);
	});
	
	getAggregationTypes(function(data){
		initAggregateFilter("aggregationType", data, updateHeatMap);
	});
	
};

function updateSequencesChart() {
	console.log("updateSequencesChart");
	
	updateFilters();
	
	getSequencesData(deviceId2, from2, to2, function(data){
		if(data.children.length==0) {
			$("#nodata").show();
		} else {
			$("#nodata").hide();
		}
		drawSequenceChart(data);
	});
};

function updateChart0() {
	console.log("updateChart0");
	
	updateFilters();
	getDataForChart(deviceId0, from0, to0, type0, function(data){
		updateChartElements(data);
		
		chart0.setTitle({text: type0});
		chart0.series[0].setData(elements, true);
	});
};

function initEventOnDay() {
	var element = $("#heatmap").find('rect').unbind('click').on('click', function(){
		var value = $(this).attr('baseDate');
		if(value) {
			updateChart1('chart1', deviceId1, value, type1);
		}
	});
};

function updateChart1(id, deviceId, baseData, type) {
	var base = moment(baseData);
	var start = encodeURIComponent(base.startOf('day').format(SERVER_FORMAT));
	var end = encodeURIComponent(base.endOf('day').format(SERVER_FORMAT));
	
	getDataForChart(deviceId, start, end, type, function(data){
		updateChartElements(data);
		
		chart1.setTitle({text: moment(baseData).format('dddd, ll')});
		chart1.series[0].setData(elements, true)
	});
};

function initTimeRange(id, callback) {
	var current = moment();
	var last7Days = moment().subtract(7, 'd');
	var last30Days = moment().subtract(30, 'd');
	var startThisMonth = moment().startOf('month');
	var startLastMonth = moment().subtract(1, 'month').startOf('month');
	var endLastMonth = moment().subtract(1, 'month').endOf('month');
	
	$('#'+id).daterangepicker({
		"timePicker": true,
	    "timePicker24Hour": true,
	    "timePickerIncrement": 1,
	    "ranges": {
	        "Last 7 Days": [
	            last7Days,
	            current
	        ],
	        "Last 30 Days": [
	            last30Days,
	            current
	        ],
	        "This Month": [
	            startThisMonth,
	            current
	        ],
	        "Last Month": [
	            startLastMonth,
	            endLastMonth
	        ]
	    },
	    "startDate": moment().startOf('year').format("MM/DD/YYYY"),
	    "endDate": moment().format("MM/DD/YYYY"),
	    "opens": "left",
	    "drops": "down",
	    "buttonClasses": "btn btn-sm",
	    "applyClass": "btn-success",
	    "cancelClass": "btn-default"
	}, function(start, end, label) {
		callback(start, end);
	});
	
	$('#' + id + ' span').html(last7Days.format(TIME_RANGE_FORMAT) + ' - ' + current.format(TIME_RANGE_FORMAT));
	from0 = from1 = from2 = encodeURIComponent(last7Days.format(SERVER_FORMAT));
	to0 = to1 = to2 = encodeURIComponent(current.format(SERVER_FORMAT));
};
