var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

function getColor(value) {
	if (!value) {
		return "white"; // default color
	}
	if (value <= -500) {
		return "2B4509";
	}
	if (value <= -200) {
		return "5B8525";
	}
	if (value <= -100) {
		return "85AD50";
	}
	if (value <= 0) {
		return "D6F2B1";
	}
	if (value <= 100) {
		return "C1C2DE";
	}
	if (value <= 200) {
		return "9B9CC4";
	}
	if (value <= 500) {
		return "7B7DAD";
	}
	if (value <= 1000) {
		return "5F6199";
	}
	if (value <= 1500) {
		return "3A3C7A";
	}
	if (value <= 2000) {
		return "2A2C63";
	}
	if (value <= 3000) {
		return "1E2052";
	}
	if (value <= 5000) {
		return "12133B";
	}
	if (value <= 7000) {
		return "0B0D2E";
	}
	if (value <= 10000) {
		return "229C32";
	}
	if (value <= 20000) {
		return "146E20";
	}
	if (value <= 50000) {
		return "0A4211";
	}
	return "04300A";
};

var rects, svg;
var thisYear = new Date().getFullYear();

function dateFormat(d) {
	return moment(d).format('dddd, ll');
};

function drawHeatMap() {
	var element = $("#heatmap");
	
	var maxYear = 5;
	var width = element.width();
	var cellSize = width / (53 + 4);
	var height = cellSize * 8;
	
	var monthPath = function(time) {
		var t1 = new Date(time.getFullYear(), time.getMonth() + 1, 0),
			d0 = +day(time), w0 = + week(time),
			d1 = +day(t1), w1 = + week(t1);
		return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
			+ "H" + w0 * cellSize + "V" + 7 * cellSize
			+ "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
			+ "H" + (w1 + 1) * cellSize + "V" + 0
			+ "H" + (w0 + 1) * cellSize + "Z";
	}
	
	var years = d3.range(thisYear - maxYear, thisYear + 1);

	var day = d3.time.format("%w"),
		week = d3.time.format("%U"),
		percent = d3.format(".1%"),
		format = d3.time.format("%Y-%m-%d");

	d3.select(element.find('div')[0]).append("svg")
		.attr("width", width)
		.attr("height", 30)
		.selectAll(".legend")
		.data(months)
		.enter()
			.append("text")
			.attr("x", function(d, i) {  return cellSize * 4.3*(i + 1); })
			.attr("y", 25)
			.text(function(d) { return d; })
			.attr("font-size", "18px");
	
	svg = d3.select(element.find('div')[0]).selectAll("svg")
		.data(years)
		.enter().append("svg")
			.attr("width", width)
			.attr("height", height)
			.attr("class", function(d) { return "RdYlGn" })
			.attr('id', function(d) { return d })
			.style('display', 'none')
		.append("g")
			.attr("transform", "translate(" + ((width - cellSize * 53) / 2) + ",0)");

	svg.append("text")
		.attr("transform", "translate(-6," + cellSize * 3.5 + ")rotate(-90)")
		.attr("font-size", "18px")
		.style("text-anchor", "middle")
		.text(function(d) { return d; });

	rects = svg.selectAll(".day")
		.data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
		.enter().append("rect")
			.attr("class", "day")
			.attr("width", cellSize)
			.attr("height", cellSize)
			.attr("x", function(d) { return week(d) * cellSize; })
			.attr("y", function(d) { return day(d) * cellSize; })
			.datum(format);

	rects.append("title")
		.text(dateFormat);

	svg.selectAll(".month")
		.data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
		.enter().append("path")
			.attr("class", "month")
			.attr("d", monthPath);
};

function updateDataHeatMap(data) {
	$("svg.RdYlGn").hide();
		
	// clean
	rects.attr('class', 'day').style('fill', '').select('title').text(dateFormat);
	
	var years = [];
	rects.filter(function(d) { // filter out none-data
		var date = moment(d);
		var result = d in data;
		result && years.indexOf(date.year()) == -1 && years.push(moment(d).year()); // mark to show element
		return result;
	}).style("fill", function(d) { 
		return getColor(Math.ceil(data[d])) // update date color
	}).attr("class", 'day')
		.attr('baseDate', function(d) { return d })
		.on({
			"mouseover": function(d) {
				d3.select(this).style("cursor", "pointer")}, 
			"mouseout": function(d) {
				d3.select(this).style("cursor", "default")}
		})
	.select('title').text(function(d){
		var value = data[d];
		var content = moment(d).format('dddd, ll') + '\n' + "Value: " + value;
		return content;
	});
	
	// only show years that have data
	if (years.length == 0) {
		years.push(thisYear);
	}
	
	svg.each(function(index) {
		if (years.indexOf(index) > -1) {
			$("#"+index).show();
		}
	});
	
	initEventOnDay();
};
