function getAllTypes(sucess) {
	var url = BASE_URL + "/seri/types";
	$.getJSON(url, function (data) {
		sucess(data);
	});
};

function getNumbericTypes(sucess) {
	var url = BASE_URL + "/seri/numbericTypes";
	$.getJSON(url, function (data) {
		sucess(data);
	});
};

function getDurationData(deviceId, from, to, type, aggregationType, sucess) {
	var url = BASE_URL + "/seri/duration"+ "?deviceId=" + deviceId + "&from=" + from + "&to=" + to + "&type=" + type + "&aggregationType=" + aggregationType;
	$.getJSON(url, function (data) {
		sucess(data);
	});
};

function getAggregationTypes(sucess) {
	var url = BASE_URL + "/seri/aggregationTypes";
	$.getJSON(url, function (data) {
		sucess(data);
	});
};

function getDataForChart(deviceId, from, to, type, sucess) {
	var url = BASE_URL + "/seri" + "?deviceId=" + deviceId + "&from=" + from + "&to=" + to + "&type=" + type;
	$.getJSON(url, function (data) {
		sucess(data);
	});
};

function getSequencesData(deviceId, from, to, sucess) {
	var url = BASE_URL + "/seri/sequences" + "?deviceId=" + deviceId + "&from=" + from + "&to=" + to;
	$.getJSON(url, function (data) {
		sucess(data);
	});
};