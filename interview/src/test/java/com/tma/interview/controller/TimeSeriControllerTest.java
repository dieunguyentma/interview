package com.tma.interview.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.tma.interview.controller.impl.TimeSeriControllerImpl;
import com.tma.interview.dao.DeviceDAO;
import com.tma.interview.dao.TimeSeriDAO;
import com.tma.interview.model.APIException;
import com.tma.interview.model.AggregationType;
import com.tma.interview.model.Device;
import com.tma.interview.model.MetricType;
import com.tma.interview.model.Sequence;
import com.tma.interview.model.TimeSeri;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class TimeSeriControllerTest {

	@InjectMocks
	private TimeSeriController controller = new TimeSeriControllerImpl();

	@Mock
	private Device device;

	@Mock
	private DeviceDAO deviceDAO;

	@Mock
	private TimeSeriDAO timeSeriDAO;

	@Mock
	private TimeSeri s1, s2, s3, s4;

	private long deviceId = 1;
	private String from = "2017-01-01T10:10:05+0700";
	private String to = "2017-01-05T10:10:05+0700";
	private List<TimeSeri> series;

	private DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
	private DateTime now = formatter.parseDateTime("05/01/2017 10:20:05");

	@Before
	public void setup() {
		Mockito.when(s1.getValue()).thenReturn("10");
		Mockito.when(s1.getTimestamp()).thenReturn(now.minusDays(1).toDate());

		Mockito.when(s2.getValue()).thenReturn("-25.5");
		Mockito.when(s2.getTimestamp()).thenReturn(now.minusDays(1).plusHours(6).toDate());

		Mockito.when(s3.getValue()).thenReturn("5");
		Mockito.when(s3.getTimestamp()).thenReturn(now.toDate());

		Mockito.when(s4.getValue()).thenReturn("3");
		Mockito.when(s4.getTimestamp()).thenReturn(now.minusHours(4).toDate());

		series = Arrays.asList(s1, s2, s3, s4);

		Mockito.when(deviceDAO.getById(deviceId)).thenReturn(device);
		Mockito.when(timeSeriDAO.get(Mockito.eq(device), Mockito.any(), Mockito.any())).thenReturn(series);
	}

	@Test
	public void testGetSequences() {
		Mockito.when(s1.getValue()).thenReturn("abc, def, ghk");
		Mockito.when(s2.getValue()).thenReturn("abc, def, ghk");
		Mockito.when(s3.getValue()).thenReturn("aaa, def, ghk");
		Mockito.when(s4.getValue()).thenReturn("bbb, ccc, ddd");
		series = Arrays.asList(s1, s2, s3, s4);

		Mockito.when(timeSeriDAO.get(Mockito.eq(device), Mockito.any(), Mockito.any())).thenReturn(series);

		Sequence sequences = controller.getSequences(deviceId, from, to);

		Assert.assertEquals(2, sequences.getChildren().size());
		Sequence country0 = sequences.getChildren().get(0);
		Assert.assertEquals("ghk", country0.getName());
		Assert.assertEquals(1, country0.getChildren().size());
		Sequence country0City0 = country0.getChildren().get(0);
		Assert.assertEquals("def", country0City0.getName());
		Assert.assertEquals(2, country0City0.getChildren().size());
		Sequence country0City0Street0 = country0City0.getChildren().get(0);
		Assert.assertEquals("abc", country0City0Street0.getName());
		Assert.assertEquals(2L, country0City0Street0.getSize(), 0);
		Sequence country0City0Street1 = country0City0.getChildren().get(1);
		Assert.assertEquals("aaa", country0City0Street1.getName());
		Assert.assertEquals(1L, country0City0Street1.getSize(), 0);

		Sequence country1 = sequences.getChildren().get(1);
		Assert.assertEquals("ddd", country1.getName());
		Assert.assertEquals(1, country1.getChildren().size());
		Sequence country1City0 = country1.getChildren().get(0);
		Assert.assertEquals("ccc", country1City0.getName());
		Assert.assertEquals(1, country1City0.getChildren().size());
		Sequence country1City0Street0 = country1City0.getChildren().get(0);
		Assert.assertEquals("bbb", country1City0Street0.getName());
		Assert.assertEquals(1L, country1City0Street0.getSize(), 0);
	}

	@Test
	public void testGetSumDurationData() {
		Map<String, Double> data = controller.getDurationData(deviceId, from, to, MetricType.NETWORK_BASED_LOCATION_RSSI, AggregationType.SUM);
		Set<String> keys = data.keySet();
		Assert.assertEquals(2, keys.size());

		Assert.assertEquals(-15.5, data.get("2017-01-04"));
		Assert.assertEquals(8.0, data.get("2017-01-05"));
	}

	@Test
	public void testGetMaxDurationData() {
		Map<String, Double> data = controller.getDurationData(deviceId, from, to, MetricType.NETWORK_BASED_LOCATION_RSSI, AggregationType.MAX);
		Set<String> keys = data.keySet();
		Assert.assertEquals(2, keys.size());

		Assert.assertEquals(10.0, data.get("2017-01-04"));
		Assert.assertEquals(5.0, data.get("2017-01-05"));
	}

	@Test
	public void testGetMinDurationData() {
		Map<String, Double> data = controller.getDurationData(deviceId, from, to, MetricType.NETWORK_BASED_LOCATION_RSSI, AggregationType.MIN);
		Set<String> keys = data.keySet();
		Assert.assertEquals(2, keys.size());

		Assert.assertEquals(-25.5, data.get("2017-01-04"));
		Assert.assertEquals(3.0, data.get("2017-01-05"));
	}

	@Test
	public void testGetAvgDurationData() {
		Map<String, Double> data = controller.getDurationData(deviceId, from, to, MetricType.NETWORK_BASED_LOCATION_RSSI, AggregationType.AVG);
		Set<String> keys = data.keySet();
		Assert.assertEquals(2, keys.size());

		Assert.assertEquals(-7.75, data.get("2017-01-04"));
		Assert.assertEquals(4.0, data.get("2017-01-05"));
	}

	@Test
	public void testDeviceNotFound() {
		try {
			controller.getDurationData(2L, from, to, MetricType.NETWORK_BASED_LOCATION_RSSI, AggregationType.AVG);
			Assert.fail();
		} catch (APIException ex) {
			Assert.assertEquals("Device not found", ex.getMessage());
		}
	}

}
