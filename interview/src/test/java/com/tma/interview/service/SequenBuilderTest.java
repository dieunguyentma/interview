package com.tma.interview.service;

import java.util.List;

import org.junit.Test;

import com.tma.interview.model.Sequence;

import junit.framework.Assert;

public class SequenBuilderTest {

	@Test
	public void testBuildData() {
		SequenceBuilder builder = new SequenceBuilder();

		builder.add(new Sequence("street"), new Sequence("city"), new Sequence("country"));
		builder.add(new Sequence("street"), new Sequence("city"), new Sequence("country"));
		builder.add(new Sequence("street1"), new Sequence("city"), new Sequence("country"));
		builder.add(new Sequence("street"), new Sequence("city"), new Sequence("country"));
		builder.add(new Sequence("street1"), new Sequence("city"), new Sequence("country"));
		builder.add(new Sequence("street2"), new Sequence("city1"), new Sequence("country"));
		builder.add(new Sequence("street3"), new Sequence("city1"), new Sequence("country"));
		builder.add(new Sequence("street2"), new Sequence("city1"), new Sequence("country"));
		builder.add(new Sequence("street1"), new Sequence("city"), new Sequence("country"));
		builder.add(new Sequence("street1"), new Sequence("city"), new Sequence("country1"));

		List<Sequence> data = builder.getData();
		
		Assert.assertEquals(2, data.size());
		Sequence s0 = data.get(0);
		
		// country
		Assert.assertEquals("country", s0.getName());
		Assert.assertEquals(2, s0.getChildren().size());
		
		// city
		List<Sequence> children = s0.getChildren();
		Sequence c0 = children.get(0);
		Assert.assertEquals("city", c0.getName());
		Assert.assertEquals(2, c0.getChildren().size());
		
		List<Sequence> c0Children = c0.getChildren();
		Assert.assertEquals(2, c0Children.size());
		
		Assert.assertEquals("street", c0Children.get(0).getName());
		Assert.assertEquals(3L, c0Children.get(0).getSize(), 0);
		Assert.assertEquals("street1", c0Children.get(1).getName());
		Assert.assertEquals(3L, c0Children.get(1).getSize(), 0);

		// city 1
		Sequence c1 = children.get(1);
		Assert.assertEquals("city1", c1.getName());
		Assert.assertEquals(2, c1.getChildren().size());
		
		List<Sequence> c1Children = c1.getChildren();
		Assert.assertEquals(2, c1Children.size());
		
		Assert.assertEquals("street2", c1Children.get(0).getName());
		Assert.assertEquals(2L, c1Children.get(0).getSize(), 0);
		Assert.assertEquals("street3", c1Children.get(1).getName());
		Assert.assertEquals(1L, c1Children.get(1).getSize(), 0);
		
		// country 1
		Sequence s1 = data.get(1);
		
		// country
		Assert.assertEquals("country1", s1.getName());
		Assert.assertEquals(1, s1.getChildren().size());
		
		List<Sequence> s1Children = s1.getChildren();
		Sequence s10 = s1Children.get(0);
		Assert.assertEquals("city", s10.getName());
		
		Assert.assertEquals(1, s10.getChildren().size());
		Sequence s100 = s10.getChildren().get(0);
		Assert.assertEquals("street1", s100.getName());
		Assert.assertEquals(1L, s100.getSize(), 0);
	}

}
